﻿namespace K_means
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ToolPanel = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.executeButton = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.LbPoints = new System.Windows.Forms.Label();
            this.PointsTrackBar = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.ClassesTrackBar = new System.Windows.Forms.TrackBar();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.timer = new System.Timers.Timer();
            this.ToolPanel.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.PointsTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.ClassesTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.timer)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolPanel
            // 
            this.ToolPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ToolPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ToolPanel.Controls.Add(this.flowLayoutPanel2);
            this.ToolPanel.Controls.Add(this.flowLayoutPanel1);
            this.ToolPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.ToolPanel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.ToolPanel.Location = new System.Drawing.Point(853, 0);
            this.ToolPanel.Name = "ToolPanel";
            this.ToolPanel.Size = new System.Drawing.Size(329, 853);
            this.ToolPanel.TabIndex = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.executeButton);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 789);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(327, 62);
            this.flowLayoutPanel2.TabIndex = 3;
            // 
            // executeButton
            // 
            this.executeButton.Location = new System.Drawing.Point(61, 3);
            this.executeButton.Margin = new System.Windows.Forms.Padding(61, 3, 61, 3);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(205, 37);
            this.executeButton.TabIndex = 0;
            this.executeButton.Text = "Кластеризировать";
            this.executeButton.UseVisualStyleBackColor = true;
            this.executeButton.Click += new System.EventHandler(this.executeButton_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.LbPoints);
            this.flowLayoutPanel1.Controls.Add(this.PointsTrackBar);
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.ClassesTrackBar);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(7, 14, 7, 14);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(327, 289);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // LbPoints
            // 
            this.LbPoints.Location = new System.Drawing.Point(14, 28);
            this.LbPoints.Margin = new System.Windows.Forms.Padding(7, 14, 7, 0);
            this.LbPoints.Name = "LbPoints";
            this.LbPoints.Size = new System.Drawing.Size(298, 30);
            this.LbPoints.TabIndex = 0;
            this.LbPoints.Text = "Число образов";
            this.LbPoints.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PointsTrackBar
            // 
            this.PointsTrackBar.Location = new System.Drawing.Point(14, 72);
            this.PointsTrackBar.Margin = new System.Windows.Forms.Padding(7, 14, 7, 14);
            this.PointsTrackBar.Maximum = 100000;
            this.PointsTrackBar.Minimum = 1000;
            this.PointsTrackBar.Name = "PointsTrackBar";
            this.PointsTrackBar.Size = new System.Drawing.Size(298, 56);
            this.PointsTrackBar.TabIndex = 1;
            this.PointsTrackBar.TickFrequency = 9900;
            this.PointsTrackBar.Value = 20000;
            this.PointsTrackBar.Scroll += new System.EventHandler(this.trackBar_Scroll);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(14, 156);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 14, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(302, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "Число классов";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ClassesTrackBar
            // 
            this.ClassesTrackBar.Location = new System.Drawing.Point(14, 193);
            this.ClassesTrackBar.Margin = new System.Windows.Forms.Padding(7, 14, 7, 14);
            this.ClassesTrackBar.Maximum = 20;
            this.ClassesTrackBar.Minimum = 2;
            this.ClassesTrackBar.Name = "ClassesTrackBar";
            this.ClassesTrackBar.Size = new System.Drawing.Size(302, 56);
            this.ClassesTrackBar.TabIndex = 3;
            this.ClassesTrackBar.TickFrequency = 2;
            this.ClassesTrackBar.Value = 6;
            this.ClassesTrackBar.Scroll += new System.EventHandler(this.trackBar_Scroll);
            // 
            // pictureBox
            // 
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(853, 853);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000D;
            this.timer.SynchronizingObject = this;
            this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1182, 853);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.ToolPanel);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "K-средних";
            this.ToolPanel.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.PointsTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.ClassesTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.timer)).EndInit();
            this.ResumeLayout(false);
        }

        private System.Timers.Timer timer;

        private System.Windows.Forms.Button executeButton;

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;

        private System.Windows.Forms.TrackBar ClassesTrackBar;

        private System.Windows.Forms.Label label1;

        private System.Windows.Forms.ToolTip toolTip;

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;

        private System.Windows.Forms.TrackBar PointsTrackBar;

        private System.Windows.Forms.Label LbPoints;

        private System.Windows.Forms.PictureBox pictureBox;

        private System.Windows.Forms.Panel ToolPanel;

        #endregion
    }
}