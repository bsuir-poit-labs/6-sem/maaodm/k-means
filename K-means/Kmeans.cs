﻿using System;
using System.Drawing;
using static System.Drawing.Color;
using static System.Math;

namespace K_means
{
    public class Kmeans
    {
        private readonly int _width;
        private readonly int _height;
        private readonly int _classesCount;

        private Point[] _centers;
        private readonly double[] _destinationsToCenters;
        private readonly int[,] _pointsClasses;

        private readonly Color[] _colors =
        {
            Red, Blue, Green, Purple, Brown, Yellow, Wheat, Cyan, Lime, Pink, Gray, SkyBlue,
            Maroon, Orange, Fuchsia, Indigo, Teal, Goldenrod, Olive, DarkGray
        };

        public Kmeans(int pointsCount, int classesCount)
        {
            _width = _height = (int) Ceiling(Sqrt(pointsCount));
            _classesCount = classesCount;

            _destinationsToCenters = new double[_classesCount];
            _pointsClasses = new int[_width, _height];

            IdentifyCenters();
        }

        private void IdentifyCenters()
        {
            var random = new Random();
            _centers = new Point[_classesCount];

            for (var i = 0; i < _classesCount; i++)
            {
                _centers[i].X = random.Next(_width);
                _centers[i].Y = random.Next(_height);

                for (var j = 0; j < i; j++)
                {
                    if (_centers[i].X == _centers[j].X && _centers[i].Y == _centers[j].Y)
                    {
                        --i;
                        break;
                    }
                }
            }
        }

        public Bitmap Cluster()
        {
            ClassifyPoints();
            var bitmap = DrawBitmap();
            CalculateCenter();

            return bitmap;
        }

        private void CalculateCenter()
        {
            var sumsX = new int[_classesCount];
            var sumsY = new int[_classesCount];
            var counts = new int[_classesCount];

            for (int x = 0; x < _width; x++)
            {
                for (int y = 0; y < _height; y++)
                {
                    sumsX[_pointsClasses[x, y]] += x;
                    sumsY[_pointsClasses[x, y]] += y;
                    counts[_pointsClasses[x, y]]++;
                }
            }

            for (int i = 0; i < _classesCount; i++)
            {
                _centers[i].X = sumsX[i] / counts[i];
                _centers[i].Y = sumsY[i] / counts[i];
            }
        }

        private void ClassifyPoints()
        {
            for (int x = 0; x < _width; x++)
            {
                for (int y = 0; y < _height; y++)
                {
                    _pointsClasses[x, y] = GetClosestCenterIndex(x, y);
                }
            }
        }

        private int GetClosestCenterIndex(int x, int y)
        {
            for (int i = 0; i < _classesCount; i++)
            {
                _destinationsToCenters[i] =
                    Sqrt(Pow(x - _centers[i].X, 2) + Pow(y - _centers[i].Y, 2));
            }

            var minDestination = _destinationsToCenters[0];
            var minIndex = 0;
            for (var i = 1; i < _classesCount; i++)
            {
                if (_destinationsToCenters[i] < minDestination)
                {
                    minDestination = _destinationsToCenters[i];
                    minIndex = i;
                }
            }

            return minIndex;
        }

        private Bitmap DrawBitmap()
        {
            var bitmap = new Bitmap(_width, _height);

            for (int x = 0; x < _width; x++)
            {
                for (int y = 0; y < _height; y++)
                {
                    bitmap.SetPixel(x, y, _colors[_pointsClasses[x, y]]);
                }
            }

            foreach (var point in _centers)
            {
                bitmap.SetPixel(point.X, point.Y, Black);
            }

            return bitmap;
        }
    }
}