﻿using System;
using System.Timers;
using System.Windows.Forms;

namespace K_means
{
    public partial class MainForm : Form
    {
        private bool _isExecuting;
        private Kmeans _kmeans;

        public MainForm()
        {
            InitializeComponent();

            toolTip.SetToolTip(PointsTrackBar, PointsTrackBar.Value.ToString());
            toolTip.SetToolTip(ClassesTrackBar, ClassesTrackBar.Value.ToString());
        }

        private void trackBar_Scroll(object sender, EventArgs e)
        {
            toolTip.SetToolTip((Control) sender, ((TrackBar) sender).Value.ToString());
        }

        private void executeButton_Click(object sender, EventArgs e)
        {
            _isExecuting = !_isExecuting;
            if (_isExecuting)
            {
                PointsTrackBar.Enabled = ClassesTrackBar.Enabled = false;
                executeButton.Text = @"Остановить";
                _kmeans = new Kmeans(PointsTrackBar.Value, ClassesTrackBar.Value);
                timer.Start();
            }
            else
            {
                PointsTrackBar.Enabled = ClassesTrackBar.Enabled = true;
                executeButton.Text = @"Кластеризировать";
                timer.Stop();
            }
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_isExecuting)
            {
                pictureBox.Image = _kmeans.Cluster();
            }
        }
    }
}